package assign.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import assign.domain.Meeting;
import assign.domain.MeetingSimple;
import assign.domain.Meetings;
import assign.domain.Project;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

public class EavesdropDBServiceImpl implements EavesdropDBService {

	String dbURL = "";
	String dbUsername = "";
	String dbPassword = "";
	DataSource ds;

	// DB connection information would typically be read from a config file.
	public EavesdropDBServiceImpl(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		
		ds = setupDataSource();
	}
	
	public DataSource setupDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUsername(this.dbUsername);
        ds.setPassword(this.dbPassword);
        ds.setUrl(this.dbURL);
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        return ds;
    }
	
	/* Will try to add a meeting to the database - Will throw an exception if anything goes wrong */
	/* with executing an insert statement with the data given                                     */
	public Meeting addMeeting(Meeting m, int projectId) throws Exception
	{
		Connection conn = ds.getConnection();
		
		String insert = "INSERT INTO meetings(name, year, project_id) VALUES(?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(insert,
                Statement.RETURN_GENERATED_KEYS);
		
		stmt.setString(1, m.getName());
		stmt.setInt(2, m.getYear());
		stmt.setInt(3, projectId);
		
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating meeting failed, no rows affected.");
        }
        
        /* Fill in the meeting id assigned */
        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
        	m.setMeetingId(generatedKeys.getInt(1));
        }
        else {
            throw new SQLException("Creating meeting failed, no ID obtained.");
        }
        
        // Close the connection
        conn.close();
        
		return m;
	}
	
	/* Will attempt to add a project to the projects table */
	/* Will throw exception if the data passed in produces */
	/* a malformed insert statement                        */
	public Project addProject(Project p) throws Exception
	{
		Connection conn = ds.getConnection();
		
		String insert = "INSERT INTO projects(name, description) VALUES(?, ?)";
		PreparedStatement stmt = conn.prepareStatement(insert,
                Statement.RETURN_GENERATED_KEYS);
		
		stmt.setString(1, p.getName());
		stmt.setString(2, p.getDescription());
		
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating course failed, no rows affected.");
        }
        
        /* Get the project id assigned */
        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
        	p.setProjectId(generatedKeys.getInt(1));
        }
        else {
            throw new SQLException("Creating course failed, no ID obtained.");
        }
        
        // Close the connection
        conn.close();
        
		return p;
	}
	
	/* Attempts to update a project in the projects table   */
	/* Will throw an exception if the data passed in is not */
	/* as expected                                          */
	public void updateProject(Project p) throws SQLException
	{
		Connection conn = ds.getConnection();
		
		String updateTableSQL = "UPDATE projects SET name = ?, description = ? WHERE project_id = ?";
		PreparedStatement preparedStatement = conn.prepareStatement(updateTableSQL,Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, p.getName());
		preparedStatement.setString(2, p.getDescription());
		preparedStatement.setInt(3, p.getProjectId());
		
		// execute insert SQL stetement
		int rowsAffected = preparedStatement .executeUpdate();
		
        if (rowsAffected == 0) {
            throw new SQLException("Updating project failed, no rows affected.");
        }
        
        // Close the connection
        conn.close();
	}
	
	/* Attempts to get a project and all off it's meetings */
	/* Will throw an exception if the data passed in does  */
	/* not produce a valid query statement                 */
	public Project getProject(int projectId) throws SQLException
	{
		Connection conn = ds.getConnection();
		String getMeetings = "select name, year, project_id from meetings where project_id = ?";
		String getProject = "select name, description, project_id from projects where project_id = ?";
		
		PreparedStatement getMeetingsStatement = conn.prepareStatement(getMeetings,Statement.RETURN_GENERATED_KEYS);
		PreparedStatement getProjectStatement = conn.prepareStatement(getProject,Statement.RETURN_GENERATED_KEYS);
		
		getMeetingsStatement.setInt(1, projectId);
		getProjectStatement.setInt(1, projectId);
		
		ResultSet meetingsResult = getMeetingsStatement.executeQuery();
		ResultSet projectResult = getProjectStatement.executeQuery();
		
		/* Get all of the projects data */
		ArrayList<MeetingSimple> meetings = new ArrayList<MeetingSimple>();
		while(meetingsResult.next())
		{
			String name = meetingsResult.getString("name");
			int year = meetingsResult.getInt("year");
			meetings.add( new MeetingSimple(name, year) );
		}
		
		/* Get the project data and add the meetings data */
		Project p = null;
		if(projectResult.next())
		{
			String name = projectResult.getString("name");
			String desc = projectResult.getString("description");
			Meetings meetingsObj = new Meetings();
			meetingsObj.setMeetings(meetings);
			p = new Project(name, desc, projectId, meetingsObj);
		}
		
		conn.close();

		return p;
	}
	
	/* Attempts to delete a project but will throw an exception     */
	/* if the data passed in does not form a valid delete statement */
	public void deleteProject(int projectId) throws SQLException
	{
		Connection conn = ds.getConnection();
		String deleteProjectsMeetingsSQL = "DELETE FROM meetings WHERE project_id = ?";
		String deleteProjectSQL = "DELETE FROM projects WHERE project_id = ?";
		
		PreparedStatement deleteMeetingsStatement = conn.prepareStatement(deleteProjectsMeetingsSQL,Statement.RETURN_GENERATED_KEYS);
		PreparedStatement deleteProjectStatement = conn.prepareStatement(deleteProjectSQL,Statement.RETURN_GENERATED_KEYS);
	
		deleteMeetingsStatement.setInt(1, projectId);
		deleteProjectStatement.setInt(1, projectId);
		
		deleteMeetingsStatement.execute();
		deleteProjectStatement.execute();
		
		conn.close();
	}
	
	/* decides if the projects exists or not */
	public boolean projectExists(int projectId) throws SQLException
	{
		Connection conn = ds.getConnection();
		String findProjectSQL = "select name from projects WHERE project_id = ?";
		
		PreparedStatement findProjectStatement = conn.prepareStatement(findProjectSQL,Statement.RETURN_GENERATED_KEYS);
	
		findProjectStatement.setInt(1, projectId);
		
		ResultSet rs = findProjectStatement.executeQuery();
		
		boolean exists = rs.next();
		
		conn.close();
		
		return exists;
	}
}
