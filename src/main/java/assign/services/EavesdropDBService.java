package assign.services;

import java.sql.SQLException;

import assign.domain.Meeting;
import assign.domain.Project;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

public interface EavesdropDBService {

	public Project addProject(Project p) throws Exception;
	
	public void updateProject(Project p) throws SQLException;
	
	public Meeting addMeeting(Meeting m, int projectId) throws Exception;
	
	public Project getProject(int projectId) throws SQLException;
	
	public void deleteProject(int projectId) throws SQLException;
	
	public boolean projectExists(int projectId) throws SQLException;
}
