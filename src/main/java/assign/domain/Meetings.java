package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

@XmlRootElement(name = "meetings")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meetings 
{		    
	    private List<MeetingSimple> meeting = null;
	    
	    public List<MeetingSimple> getMeetings() {
	    	return meeting;
	    }
	    
	    public void setMeetings(List<MeetingSimple> meetings) {
	    	this.meeting = meetings;
	    }
}
