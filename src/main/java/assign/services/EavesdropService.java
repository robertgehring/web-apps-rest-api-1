package assign.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.net.URLEncoder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

public class EavesdropService {

	private String eavesdropUrlMeetings;
	private String eavesdropUrlLogs;
	
	public EavesdropService()
	{
		eavesdropUrlMeetings = "http://eavesdrop.openstack.org/meetings/";
		eavesdropUrlLogs = "http://eavesdrop.openstack.org/irclogs/";
	}
	
	public String getData() {
		return "Hello from Eavesdrop service.";
	}
	
	/* Purpose is to get all the names of the meetings from the Eavesdrop website */
	/* Will return an arraylist containing all the names or                       */
	/* Will return null if there is a connection error                            */
	public ArrayList<String> getAllMeetings()
	{
		ArrayList<String> projects = new ArrayList<String>();
		
	  	Document d;
	  	/* Try to get all the data from the meetings page */
    	try { d = Jsoup.connect(eavesdropUrlMeetings).get(); }
    	catch (IOException e) { return null; }
    	
    	/* Strip off the titles (first 5) and grab the names and put them in the list */
    	Elements projectElements = d.select("A[href]");
    	for(int i=5;i<projectElements.size();i++)
    		projects.add(projectElements.get(i).text());
    	
		return projects;
	}
	
	/* Purpose is to get all the names of the logs from the Eavesdrop website */
	/* Will return an arraylist containing all the names or                       */
	/* Will return null if there is a connection error                            */
	public ArrayList<String> getAllLogs()
	{
		ArrayList<String> logs = new ArrayList<String>();
		
	  	Document d;
	  	/* Try to get all of the data from the logs page */
    	try { d = Jsoup.connect(eavesdropUrlLogs).get(); }
    	catch (IOException e) { return null; }
    	
    	/* Strip off the titles (first 5) and grab the names and put them in the list */
    	Elements projectElements = d.select("A[href]");
    	for(int i=5;i<projectElements.size();i++)
    		logs.add(projectElements.get(i).text());
    	
		return logs;
	}

	/* Get all of the links that pertain to a logId from Eavesdrop */
	/* Return a list if the logId was found                        */
	/* Returns null if the logId does not exists                   */
	public ArrayList<String> getIrcLogLinks(String ircLogId) throws UnsupportedEncodingException 
	{
		ArrayList<String> links = new ArrayList<String>();
		
		Document d;
		
		/* Try to get the data from Eavesdrop */
		try { d = Jsoup.connect(eavesdropUrlLogs + URLEncoder.encode(ircLogId,"UTF-8")).get(); }
		catch (IOException e) { return null; /* Log does not exist */ }
		
		/* Get all the log's children and append them to the base url, add to list */
    	Elements logLinksIds = d.select("A[href]");
    	for(int i=5;i<logLinksIds.size();i++)
    		links.add(eavesdropUrlLogs + URLEncoder.encode(ircLogId,"UTF-8") + "/" + logLinksIds.get(i).text());
		
		return links;
	}
	
}
