package assign.resources;


import java.io.IOException;
import java.util.Scanner;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

public class TestProjectResources extends XMLTestCase
{	
	@Test
	public void testListedProjectsF() throws IOException, SAXException
	{
		ResteasyClient client = new ResteasyClientBuilder().build();
		Response response = client.target("http://localhost:8080/assignment4/myeavesdrop/projects/").request().get();

		assertEquals(200, response.getStatus());
		
		/* Get control and test XML to compare */
		Document controlDoc = XMLUnit.buildControlDocument(ControlXmlGlobals.listedProjectsControlXml);
		Document testDoc = XMLUnit.buildTestDocument(response.readEntity(String.class));
		
		assertXMLEqual(controlDoc, testDoc);
		response.close();
	}
	
	@Test
	public void testExistingLog() throws IOException, SAXException
	{
		ResteasyClient client = new ResteasyClientBuilder().build();
		Response response = client.target("http://localhost:8080/assignment4/myeavesdrop/projects/%23openstack-api/irclogs").request().get();
		
		assertEquals(200, response.getStatus());
		
		/* Get control and test XML to compare */
		Document controlDoc = XMLUnit.buildControlDocument(ControlXmlGlobals.openstackAPIXML);
		/* Trim the dates in case more logs were added since our saved state we are testing against */
		Document testDoc = XMLUnit.buildTestDocument(trimDates(response.readEntity(String.class), "2015-04-05"));

		assertXMLEqual(controlDoc, testDoc);
		response.close();

	}
	
	/* Only accepts the dates before endDate                                              */
	/* Other lines that represent links that contain a date after endDate will be ignored */
	private String trimDates(String xml, String endDate)
	{
		String xmlRevised = "";
		
		Scanner xmlScanner = new Scanner(xml);
		
		while(xmlScanner.hasNextLine())
		{
			String nextLine = xmlScanner.nextLine();
			if(beforeEndDate(nextLine, endDate))
				xmlRevised += nextLine + "\n";
		}
		
		xmlScanner.close();
		return xmlRevised;
	}
	
	/* Trims links from the xml that occured after date */
	private boolean beforeEndDate(String xmlLine, String date)
	{
		try
		{
			if(xmlLine.contains("link")) /* If it's a link check it's date */
			{
				String xmlLineDate = xmlLine.substring(xmlLine.length() - 21, xmlLine.length() - 11);
				
				int xmlLineYear = Integer.valueOf(xmlLineDate.substring(0,4));
				int xmlLineMonth = Integer.valueOf(xmlLineDate.substring(5,7));
				int xmlLineDay = Integer.valueOf(xmlLineDate.substring(8,10));
				int dateYear = Integer.valueOf(date.substring(0,4));
				int dateMonth = Integer.valueOf(date.substring(5,7));
				int dateDay = Integer.valueOf(date.substring(8,10));
				
				/* Decide if the date is beyond the cutoff */
				if ( xmlLineYear > dateYear ) 
					return false;
				else if ( xmlLineYear == dateYear )
				{
					if( xmlLineMonth > dateMonth )
						return false;
					else if ( xmlLineMonth == dateMonth )
					{
						if ( xmlLineDay > dateDay )
							return false;
					}
				}
			}
		}
		catch(StringIndexOutOfBoundsException e) { return true; /* Shouldnt get here */}
		
		return true;
	}
	
	@Test
	public void testNonExistingLog()
	{
		ResteasyClient client = new ResteasyClientBuilder().build();
		Response response = client.target("http://localhost:8080/assignment4/myeavesdrop/projects/non-existent-project/irclogs").request().get();
		
		assertEquals(404, response.getStatus());
	}
}