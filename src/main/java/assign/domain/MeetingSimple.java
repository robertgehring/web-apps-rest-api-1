package assign.domain;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

public class MeetingSimple 
{
	private String name;
	private int year;
	
	public MeetingSimple() {}
	
	public MeetingSimple(String name, int year) { this.name = name; this.year = year; }
	
	public void setName(String name) { this.name = name; }
	
	public String getName() { return this.name; }
	
	public void setYear(int year) { this.year = year; }
	
	public int getYear() { return this.year; }
}
