package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

@XmlRootElement(name = "project")
@XmlType (propOrder={"projectId","name","description", "meetings", "link"})
public class Project {

	private String name;
	private String description;
	private int projectId;
	private Meetings meetings;
		
	List<String> link = null;
	 
	public Project() {};
	
	public Project(String name, String description)
	{
		this.name = name;
		this.description = description;
	}
	
	public Project(String name, String description, int projectId)
	{
		this.name = name;
		this.description = description;
		this.projectId = projectId;
	}
	
	public Project(String name, String description, int projectId, Meetings meetings)
	{
		this.name = name;
		this.description = description;
		this.projectId = projectId;
		this.meetings = meetings;
	}
	
	@XmlAttribute(name = "id")
	public void setProjectId(int id) { projectId = id; }
	
	public int getProjectId(){ return projectId; }
	
	@XmlElement(name = "name")
	public void setName(String name) { this.name = name; }
	
	public String getName() { return name; }
	
	@XmlElement(name = "description")
	public void setDescription(String d) { this.description = d; }
	
	public String getDescription(){ return description; }
	
	public void setMeetings(Meetings m) { this.meetings = m; }
	
	public Meetings getMeetings() { return this.meetings; }
	
	public String toString() { return "Project: " + name + "\nDescription: " + description; }
	
	public List<String> getLink() { return link; }
 
    public void setLink(List<String> link) { this.link = link; }
    
    public String toBaseXml()
    {
    	return "<project>\n\t<name>"+name+"</name>\n\t<description>"+description+"</description>\n\t<id>"+
    				projectId+"</id>\n</project>";
    }
}
