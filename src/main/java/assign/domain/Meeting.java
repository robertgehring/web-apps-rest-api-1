package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

@XmlRootElement(name = "meeting")
public class Meeting {

	private String name;
	private int year;
	private int projectId;
	private int meetingId;
		
	List<String> link = null;
	 
	public Meeting() 
	{
		this.name = null;
		this.year = -1;
		this.projectId = -1;
		this.meetingId = -1;
	};
	
	public Meeting(String name, int year)
	{
		this.name = name;
		this.year = year;
		this.projectId = -1;
		this.meetingId = -1;
	}
	
	public Meeting(String name, int year, int projectId)
	{
		this.name = name;
		this.year = year;
		this.projectId = projectId;
		this.meetingId = -1;
	}
	
	public Meeting(String name, int year, int projectId, int meetingId)
	{
		this.name = name;
		this.year = year;
		this.projectId = projectId;
		this.meetingId = meetingId;
	}
	
	@XmlElement(name = "name")
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setMeetingId(int id)
	{
		meetingId = id;
	}
	
	public int getMeetingId()
	{
		return meetingId;
	}
	
	public void setProjectId(int pId)
	{
		this.projectId = pId;
	}
	
	public int getProjectId()
	{
		return this.projectId;
	}

	
	@XmlElement(name = "year")
	public void setYear(int year) {
		this.year = year;
	}
	
	public int getYear()
	{
		return year;
	}
	
	public String toString()
	{
		return "meeting: " + name + "\nyear: " + year + "\nproject_id: " + projectId + "\nmeeting_id: " + meetingId;
	}
	
	public String toBaseXml()
	{
		return "<meeting>\n\t<name>"+name+"</name>\n\t<year>"+year+"</year>\n\t<meeting_id>"+meetingId+
				"</meeting_id>\n\t<project_id>"+projectId+"</project_id>\n</meeting>";
	}
	public List<String> getLink() {
        return link;
    }
 
    public void setLink(List<String> link) {
        this.link = link;
    }
}