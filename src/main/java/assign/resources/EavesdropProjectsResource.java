package assign.resources;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.net.URLEncoder;

import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Project;
import assign.domain.Projects;
import assign.services.EavesdropService;

@Path("/myeavesdrop/projects")
public class EavesdropProjectsResource {
	
	EavesdropService eavesdropService;
	
	public EavesdropProjectsResource() {
		this.eavesdropService = new EavesdropService();
	}

	@GET
	@Path("")
	@Produces("application/xml")
	public StreamingOutput getAllProjects() throws Exception {
				
		final Projects projects = new Projects();
		
		/* Get all of the meetings and logs */
		Set<String> projectNames = new HashSet<String>();
		projectNames.addAll(this.eavesdropService.getAllMeetings());
		projectNames.addAll(this.eavesdropService.getAllLogs());
		
		/* Fill our Projects object with all of the meetings and logs names */
		projects.setProjects(new ArrayList<String>(projectNames));		
			    
	    return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProjects(outputStream, projects);
	         }
	      };	    
	}
	
	@GET
	@Path("{id}/irclogs")
	@Produces("application/xml")
	public StreamingOutput getIrlProject(@PathParam("id") String ircLogId) throws Exception
	{
		final Project log = new Project();
		log.setName(URLEncoder.encode(ircLogId,"UTF-8"));
		
		/* Get all the links that go with the logId from Eavesdrop */
		log.setLink(this.eavesdropService.getIrcLogLinks(ircLogId));
		
		if(log.getLink() == null)
			throw new NotFoundException();

	    return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProjects(outputStream, log);
	         }
	      };
	}
	
	protected void outputProjects(OutputStream os, Projects projects) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(projects, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}	
	
	protected void outputProjects(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
}
