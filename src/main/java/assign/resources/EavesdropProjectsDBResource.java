package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jboss.resteasy.spi.BadRequestException;

import assign.domain.Meeting;
import assign.domain.Project;
import assign.services.EavesdropDBService;
import assign.services.EavesdropDBServiceImpl;

/* Robert Gehring (UTEid: rjg2358)               */
/* Modern Web Apps - University of Texas, Austin */
/* Spring 2015                                   */

@Path("/myeavesdrop")
public class EavesdropProjectsDBResource {
	
	@Context org.jboss.resteasy.spi.HttpResponse response;
	EavesdropDBService eavesdropDBService;
	String password;
	String username;
	String dburl;
	
	public EavesdropProjectsDBResource(@Context ServletContext servletContext) {		
		dburl = servletContext.getInitParameter("DBURL");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		this.eavesdropDBService = new EavesdropDBServiceImpl(dburl, username, password);
	}
	
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		System.out.println("DB creds are:");
		System.out.println("DBURL:" + dburl);
		System.out.println("DBUsername:" + username);
		System.out.println("DBPassword:" + password);		
		return "Hello world " + dburl + " " + username + " " + password; 		
	}
	
	@POST
	@Path("/projects")
	@Consumes("application/xml")
	public Response postProject(Project p) throws Exception
	{
		
		/* Input validation - Make sure the name and description are not empty */
		if(p == null || p.getDescription() == null || p.getDescription().isEmpty() || p.getName() == null || p.getName().isEmpty())
		{
			return Response.status(400).build();
		}
		
		try /* Add the project to the projects table */
		{
			Project addedProject = eavesdropDBService.addProject(p);
			response.getOutputHeaders().putSingle("Location header", "http://localhost:8080/assignment5/myeavesdrop/projects/" + addedProject.getProjectId());
		}
		catch(Exception e) /* If there was a problem executing the SQL with the given parameters */
		{
			return Response.status(400).entity(e.toString()).build();
		}
		
		return Response.status(201).entity(p.toBaseXml()).build();
	}
	 
	@PUT
	@Path("projects/{id}")
	@Consumes("application/xml")
	public Response putProject(Project p, @PathParam("id") String projectId) throws Exception
	{
		/* Input validation - Make sure the name and description are not empty */
		if(p == null || p.getDescription() == null || p.getDescription().isEmpty() || p.getName() == null || p.getName().isEmpty())
		{
			return Response.status(400).build();
		}
		
		/* Input validation - the path param is actually a number */
		try { p.setProjectId(Integer.parseInt(projectId)); }
		catch(Exception e) { return Response.status(400).entity("A project id must be a number.").build(); }
		
		try { eavesdropDBService.updateProject(p); } /* Try the update */
		catch (SQLException e) /* Catch a SQL error */
		{
			return Response.status(400).build();
		}
		
		return Response.status(204).build();
	}
	
	@POST
	@Path("projects/{id}/meetings")
	@Consumes("application/xml")
	public Response postMeeting(Meeting m, @PathParam("id") String projectId) throws Exception
	{
		/* Input validation - year and name are not empty. 0 is empty and -1 is not passed in */
		if(m == null || m.getYear() == 0 || m.getYear() == -1 || m.getName() == null || m.getName().isEmpty())
		{
			return Response.status(400).build();
		}
		
		int id;
		try { id = Integer.parseInt(projectId); }
		catch (Exception e) { return Response.status(400).entity("A project id must be a number").build(); }
		
		Meeting mReturned = null;
		try /* Add the meeting */
		{
			mReturned = eavesdropDBService.addMeeting(m, id); 
			mReturned.setProjectId(id); 
		}
		catch(Exception e)
		{
			return Response.status(400).build();
		}
		
		return Response.status(201).entity(mReturned.toBaseXml()).build();
	}
	
	@GET
	@Path("/projects/{id}")
	@Produces("application/xml")
	public StreamingOutput getProject(@PathParam("id") String projectId) throws NumberFormatException, SQLException
	{
		int id;
		try { id = Integer.parseInt(projectId); }
		catch (Exception e) { throw new BadRequestException("Project Id must be a number."); }
		
		/* Input validation - See if project exists */
		if(!eavesdropDBService.projectExists(id)) { throw new NotFoundException();  }
		
		final Project p = eavesdropDBService.getProject(Integer.parseInt(projectId));
		
	    return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProject(outputStream, p);
	         }
	      };
	}
	
	@DELETE
	@Path("/projects/{id}")
	public Response deleteProject(@PathParam("id") String projectId) throws NumberFormatException, SQLException
	{
		int id;
		try { id = Integer.parseInt(projectId); }
		catch (Exception e) { return Response.status(400).entity("A project id must be a number").build(); }
		
		/* Input validation - See if project exists */
		if(!eavesdropDBService.projectExists(id))
			return Response.status(404).entity("A project with project_id "+id+" does not exist").build();
		
		/* Removes all the meetings and project */
		eavesdropDBService.deleteProject(Integer.parseInt(projectId));
		
		return Response.status(200).build();
	}
	
	protected void outputProject(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
}
